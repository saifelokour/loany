defmodule Loany.Repo.Migrations.CreateLoanApplications do
  use Ecto.Migration

  def change do
    create table(:loan_applications) do
      add :amount, :float
      add :name, :string
      add :phone_number, :string
      add :email, :string
      add :denied, :boolean, default: false, null: false
      add :interest_rate, :float

      timestamps()
    end

  end
end
