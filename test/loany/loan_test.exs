defmodule Loany.LoanTest do
  use Loany.DataCase

  alias Loany.Loan

  describe "loan_applications" do
    alias Loany.Loan.Application

    @valid_attrs %{amount: 120.5, denied: true, email: "some email", interest_rate: 120.5, name: "some name", phone_number: "some phone_number"}
    @update_attrs %{amount: 456.7, denied: false, email: "some updated email", interest_rate: 456.7, name: "some updated name", phone_number: "some updated phone_number"}
    @invalid_attrs %{amount: nil, denied: nil, email: nil, interest_rate: nil, name: nil, phone_number: nil}

    def application_fixture(attrs \\ %{}) do
      {:ok, application} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Loan.create_application()

      application
    end

    test "list_loan_applications/0 returns all loan_applications" do
      application = application_fixture()
      assert Loan.list_loan_applications() == [application]
    end

    test "get_application!/1 returns the application with given id" do
      application = application_fixture()
      assert Loan.get_application!(application.id) == application
    end

    test "create_application/1 with valid data creates a application" do
      assert {:ok, %Application{} = application} = Loan.create_application(@valid_attrs)
      assert application.amount == 120.5
      assert application.denied == true
      assert application.email == "some email"
      assert application.interest_rate == 120.5
      assert application.name == "some name"
      assert application.phone_number == "some phone_number"
    end

    test "create_application/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Loan.create_application(@invalid_attrs)
    end

    test "update_application/2 with valid data updates the application" do
      application = application_fixture()
      assert {:ok, %Application{} = application} = Loan.update_application(application, @update_attrs)
      assert application.amount == 456.7
      assert application.denied == false
      assert application.email == "some updated email"
      assert application.interest_rate == 456.7
      assert application.name == "some updated name"
      assert application.phone_number == "some updated phone_number"
    end

    test "update_application/2 with invalid data returns error changeset" do
      application = application_fixture()
      assert {:error, %Ecto.Changeset{}} = Loan.update_application(application, @invalid_attrs)
      assert application == Loan.get_application!(application.id)
    end

    test "delete_application/1 deletes the application" do
      application = application_fixture()
      assert {:ok, %Application{}} = Loan.delete_application(application)
      assert_raise Ecto.NoResultsError, fn -> Loan.get_application!(application.id) end
    end

    test "change_application/1 returns a application changeset" do
      application = application_fixture()
      assert %Ecto.Changeset{} = Loan.change_application(application)
    end
  end
end
