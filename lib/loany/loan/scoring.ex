defmodule Loany.Loan.Scoring do
  alias Loany.Loan.Store

  def score(%{"amount" => amount} = application) do
    {parsed_amount, _} = Integer.parse(amount)
    application = Map.put(application, "amount", parsed_amount)
    rejected? =
      Store.get_all_applications()
      |> Enum.any?(fn %{amount: prev_amount} -> parsed_amount < prev_amount end)

    score(application, rejected?)
  end

  defp score(application, true), do: {:error, Map.put_new(application, "denied", true)}
  defp score(%{"amount" => amount} = application, false) do
    if is_prime(amount) do
      {:ok, Map.put_new(application, "interest_rate", 9.99)}
    else
      {:ok, Map.put_new(application, "interest_rate", Enum.random(400..1200) / 100)}
    end
  end

  defp is_prime(n) when n in [2, 3], do: true
  defp is_prime(n) do
    floored_sqrt =
      :math.sqrt(n)
      |> Float.floor
      |> round
    !Enum.any?(2..floored_sqrt, &(rem(n, &1) == 0))
  end
end
