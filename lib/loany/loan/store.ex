defmodule Loany.Loan.Store do
  use Agent

  def start_link(opts) do
    {initial_value, opts} = Keyword.pop(opts, :initial_value, [])
    Agent.start_link(fn -> initial_value end, opts)
  end

  def get_all_applications do
    Agent.get(__MODULE__, & &1)
  end

  def get_last_application do
    Agent.get(__MODULE__, &hd(&1))
  end

  def put_application(new_application) do
    Agent.update(__MODULE__, fn all_applications ->
      [new_application | all_applications]
    end)
  end
end
