defmodule Loany.Loan.Application do
  use Ecto.Schema
  import Ecto.Changeset

  schema "loan_applications" do
    field :amount, :float
    field :denied, :boolean, default: false
    field :email, :string
    field :interest_rate, :float, default: 0.0
    field :name, :string
    field :phone_number, :string

    timestamps()
  end

  @doc false
  def changeset(application, attrs) do
    application
    |> cast(attrs, [:amount, :name, :phone_number, :email, :denied, :interest_rate])
    |> validate_required([:amount, :name, :phone_number, :email, :denied, :interest_rate])
  end

end
