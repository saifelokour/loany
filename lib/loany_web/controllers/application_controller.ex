defmodule LoanyWeb.ApplicationController do
  use LoanyWeb, :controller

  alias Loany.Loan
  alias Loany.Loan.Application
  alias Loany.Loan.Store
  alias Loany.Loan.Scoring

  def all(conn, _params) do
    loan_applications = Loan.list_loan_applications()
    render(conn, "all.html", loan_applications: loan_applications)
  end

  def new(conn, _params) do
    changeset = Loan.change_application(%Application{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"application" => application_params}) do
    {action, updated_application} =
      case Scoring.score(application_params) do
        {:ok, scored_application} ->
          {:offer, scored_application}
        {:error, denied_application} ->
          {:deny, denied_application}
      end

    case Loan.create_application(updated_application) do
      {:ok, application} ->
        Store.put_application(application)
        conn
        |> redirect(to: Routes.application_path(conn, action))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def offer(conn, _params) do
    application = Store.get_last_application()
    render(conn, "offer.html", application: application)
  end

  def deny(conn, _params) do
    render(conn, "deny.html")
  end
end
